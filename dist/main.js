async function sleep(msec) {
  return new Promise(resolve => setTimeout(resolve, msec));
}

function arrayToHexString(arr) {
  let result = ""
  for (let i = 0; i < arr.length; i++) {
    const val = arr[i];
    if (val < 16) {
      result += '0'
    }
    result += val.toString(16);
  }
  return result.toUpperCase();
}

class NFC {
  constructor() {
    this.device = null;
    this.running = false;
  }

  static get ACK() {
    return [0x00, 0x00, 0xff, 0x00, 0xff, 0x00];
  }
  
  async open() {
    this.device = device;
  }

  async send(data) {
    let uint8a = new Uint8Array(data);
    console.debug(">>>>>>>>>>");
    console.debug(uint8a);
    await this.device.transferOut(2, uint8a);
    await sleep(10);
  }

  async sendCommand(cmd, params) {
    let command = [0x00, 0x00, 0xff, 0xff, 0xff];
    let data = [0xd6, cmd].concat(params);
    command = command.concat([data.length, 0, 256 - data.length]);
    command = command.concat(data);
    let sum = 0;
    for (let i = 0; i < data.length; i++) {sum += data[i]}
    let parity = ( 256 - sum) % 256 + 256
    command = command.concat([parity, 0]);
    await this.send(command);
    // Read resp
    await this._receive(6);
    // Read result
    return await this._receive(40);
  }

  async _receive(len) {
    console.debug("<<<<<<<<<<" + len);
    let data = await this.device.transferIn(1, len);
    console.debug(data);
    await sleep(10);
    let arr = [];
    for (let i = data.data.byteOffset; i < data.data.byteLength; i++) {
      arr.push(data.data.getUint8(i));
    }
    console.debug(arr);
    return arr;
  }

  async receive(len) {
    await this._receive(6);
    return await this._receive(len);
  }

  /**
   * Get IDm from Type 3 NFC card
   * 
   * Original from https://github.com/saturday06/webusb-felica
   */
  async getType3TagInfo() {
    // Level 9:nfc.clf.rcs380:SetCommandType 01
    await this.sendCommand(0x2a, [0x01]);
  
    // Level 9:nfc.clf.rcs380:SwitchRF 00
    await this.sendCommand(0x06, [0x00]);
  
    // Level 9:nfc.clf.rcs380:InSetRF 01010f01
    await this.sendCommand(0x00, [0x01, 0x01, 0x0f, 0x01]);
  
    // Level 9:nfc.clf.rcs380:InSetProtocol 00180101020103000400050006000708080009000a000b000c000e040f001000110012001306
    await this.sendCommand(0x02, [0x00, 0x18, 0x01, 0x01, 0x02, 0x01, 0x03, 0x00, 0x04, 0x00, 0x05, 0x00, 0x06, 0x00, 0x07, 0x08, 0x08, 0x00, 0x09, 0x00, 0x0a, 0x00, 0x0b, 0x00, 0x0c, 0x00, 0x0e, 0x04, 0x0f, 0x00, 0x10, 0x00, 0x11, 0x00, 0x12, 0x00, 0x13, 0x06]);
  
    // Level 9:nfc.clf.rcs380:InSetProtocol 0018
    await this.sendCommand(0x02, [0x00, 0x18]);
  
    // Level 9:nfc.clf.rcs380:InCommRF 6e000600ffff0100
    const data = await this.sendCommand(0x04, [0x6e, 0x00, 0x06, 0x00, 0xff, 0xff, 0x01, 0x00]);
    return {
      idm: arrayToHexString(data.slice(17, 25)),
      pmm: arrayToHexString(data.slice(25, 33)),
    }
  }

  /**
   * Get IDm from Type 2 NFC card
   * 
   * (It is main implements in this project)
   */
  async getType2TagInfo() {
    console.log('SwitchRF');
    await this.sendCommand(0x06, [0x00]);

    console.log('InSetRF');
    await this.sendCommand(0x00, [0x02, 0x03, 0x0f, 0x03]);

    console.log('InSetProtocol');
    await this.sendCommand(0x02, [0x00, 0x18, 0x01, 0x01, 0x02, 0x01, 0x03, 0x00, 0x04, 0x00, 0x05, 0x00, 0x06, 0x00, 0x07, 0x08, 0x08, 0x00, 0x09, 0x00, 0x0a, 0x00, 0x0b, 0x00, 0x0c, 0x00, 0x0e, 0x04, 0x0f, 0x00, 0x10, 0x00, 0x11, 0x00, 0x12, 0x00, 0x13, 0x06]);
    await this.sendCommand(0x02, [0x01, 0x00, 0x02, 0x00, 0x05, 0x01, 0x00, 0x06, 0x07, 0x07]);

    console.log('InCommRF:SENS');
    let sensRes = await this.sendCommand(0x04, [0x36, 0x01, 0x26]);

    console.log('InCommRF:SDD');
    await this.sendCommand(0x02, [0x04, 0x01, 0x07, 0x08]);
    await this.sendCommand(0x02, [0x01, 0x00, 0x02, 0x00]);
    const ssdRes = await this.sendCommand(0x04, [0x36, 0x01, 0x93, 0x20]);
    return arrayToHexString(ssdRes.slice(15, 19))
  }
}

/* *************************************

************************************* */
let initButton = document.getElementById('init_nfc');
let type2Button = document.getElementById('nfc_type2');
let type3Button = document.getElementById('nfc_type3');

let nfc = new NFC();
initButton.addEventListener('click', async () => {
  try {
    nfc.device = await navigator.usb.requestDevice({ filters: [{
      vendorId: 0x054c,
      protocolCode: 0x01
    }]});
    await nfc.device.open();
    await nfc.device.selectConfiguration(1);
    await nfc.device.claimInterface(0);
  } catch (e) {
    console.log(e);
  }
})

type2Button.addEventListener('click', async (e) => {
  if (nfc.device == null) {
    console.log('Device is not opened.');
    return;
  }
  console.log('Start session');
  await nfc.send(NFC.ACK);

  console.debug('GetProperty');
  await nfc.sendCommand(0x2a, [0x01]);
  console.debug('GetFirmwareVersion');
  await nfc.sendCommand(0x20, []);
  console.debug('GetPDDataVersion');
  await nfc.sendCommand(0x22, []);

  const tag = await nfc.getType2TagInfo();
  console.log(tag);
  alert(tag);
});

type3Button.addEventListener('click', async (e) => {
  if (nfc.device == null) {
    console.log('Device is not opened.');
    return;
  }
  console.log('Start session');
  await nfc.send(NFC.ACK);

  console.debug('GetProperty');
  await nfc.sendCommand(0x2a, [0x01]);
  console.debug('GetFirmwareVersion');
  await nfc.sendCommand(0x20, []);
  console.debug('GetPDDataVersion');
  await nfc.sendCommand(0x22, []);

  const tag = await nfc.getType3TagInfo();
  console.log(tag);
  alert(tag);
});
