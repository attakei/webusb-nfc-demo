# WebUSBでNFC規格カードのIDを取得するデモ


## 手法のオリジナル

* [デモサイト](https://saturday06.github.io/webusb-felica/demo.html)
* [GitHubのソース](https://github.com/saturday06/webusb-felica)
* [Qiitaの記事](https://qiita.com/saturday06/items/333fcdf5b3b8030c9b05)

##　何をするか
